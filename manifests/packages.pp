# Install KDC-specific packages.

# $include_libsasl2_modules_ldap: if set to 'present' (the default) will install
# the libsasl2-modules-ldap package, otherwise will ensure that the package is NOT
# installed. Why? The libsasl2-modules-ldap installs a SASL LDAP plugin that is
# automatically loaded by SASL and, in particular, is called by krb5-sync which uses
# SASL. Since we don't configure this plugin it generates many messages of the form
#
#   ldapdb_canonuser_plug_init() failed in sasl_canonuser_add_plugin(): invalid parameter supplied
#   _sasl_plugin_load failed on sasl_canonuser_init for plugin: ldapdb
#
# in the syslog. The only process calling SASL is krb5-sync and does not
# need this SASL plugin, so to avoid getting these annying messages in the syslog we
# provide the option of not installing the package.

class kdc::packages (
  Enum['present', 'absent'] $libsasl2_modules_ldap = 'present',
) {

  # Install Kerberos KDC packages and switch the GSS-API plugin from MIT
  # Kerberos to Heimdal.
  package {
    'stanford-server-kdc':             ensure => present;
    'kdc-monitor-replication':         ensure => present;
    'heimdal-clients':                 ensure => present;
    'heimdal-kdc':                     ensure => present;
    'heimdal-docs':                    ensure => present;
    'libsasl2-modules-gssapi-heimdal': ensure => present;
    'libsasl2-modules-gssapi-mit':     ensure => absent;
    'libsasl2-modules-ldap':           ensure => $libsasl2_modules_ldap;
  }

}

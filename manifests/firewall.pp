# Set up host-based firewall rules.
#
# 1. Allow full propagation from the master KDC (hprop) .
# 2. Allow slave to connect to master for incremental propagation (iprop).
# 3. Allow Kerberos administration from all slave KDCS as well as certain
#    subnets on campus that we trust.
# 4. Allow EVERYONE to make ticket requests.
# 5. Allow EVERYONE to make password changes.
#
# We also use the hosts.allow to restrict access (belt-and-suspenders).
#
# $slaves: An array of IP addresses or hostnames that represent all the
#   slaves in the environment. These entries will be added to the
#   sshd section of the hosts.allow file allowing them to ssh into the
#   KDC.
#
#   Furthermore, they and the array from the $trusted_kadmin_cidrs will be
#   added to the hosts.allow kadmind section to be allowed to access the
#   kadmind service.
#
#   It is safe to include non-fully-qualified hostnames in the $slaves array
#   as any entries of the array $slaves that are not fully-qualified will
#   be filtered out. (For example, Docker container names).
#
#   Note that any entries that represent IP addresses must be compatible
#   with the hosts.allow IP address format (see 'man hosts.allow' for
#   details).
#
#   Example:
#       class {'kdc::firewall':
#          slaves => ['kdc-uat-1', 'kerberos-uat2.stanford.edu', '192.168.'],
#       }
#
#    We do not require $slaves to be non-empty in case we have a master
#    with no slaves.
#
#   Default: empty

class kdc::firewall(
  $master_ip        = undef,
  $master_fqdn      = undef,
  $krb5_port        = 88,
  $slaves           = [],
  $slave_cidrs      = [],
  $trusted_kadmin_cidrs
                    = [],
  $hosts_allow_deny = true,
  $extra_sshd_hosts_allow = [],
  $extra_hpropd_cidrs     = [],
){

  if (!$master_ip) {
    fail("missing required parameter 'master_ip'")
  }
  if (!$master_fqdn) {
    fail("missing required parameter 'master_fqdn'")
  }

  # Note that we do not require $slaves and $slave_cidrs to be non-empty
  # in case we have a master with no slaves.

  # Add $extra_hpropd_hosts_allow to the master ip.
  if (size($extra_hpropd_cidrs) > 0) {
    $hprop_allowed = [$master_ip] + $extra_hpropd_cidrs
    $hprop_description = 'Allow Kerberos master (and some others) to propagate full database to slaves'
  } else {
    $hprop_allowed = [$master_ip]
    $hprop_description = 'Allow Kerberos master to propagate full database to slaves'
  }

  ## Get all the IP addresses associated with $slaves.

  # We only take the _first_ A record. This code requires the Puppet
  # module dalen/dnsquery.
  $slave_ips_raw = $slaves.map |$slave| {
    if ($slave =~ /[.]/) {
      dns_a($slave)
    }
  }

  # The above result might containe arrays within arrays, so we flatten.
  $slave_ips_flattened = flatten($slave_ips_raw)

  # Remove any 'undef's that might have crept in.
  $slave_ips = $slave_ips_flattened.filter |$ip| { $ip != undef }

#  # Construct an array of wallet server IP addresses from
#  # $wallet_server_backups. We take the first A record only.
#  $wallet_server_ips_raw = $wallet_server_backups.map |$wallet_server| {
#    if ($wallet_server =~ /[.]/) {
#      dns_a($wallet_server)
#    }
#  }

#  # Filter out some ssh lines. This template uses $wallet_server_ips_raw.
#  file { '/etc/filter-syslog/ssh-kdc-extra':
#    content => template('kdc/etc/filter-syslog/ssh-kdc-extra.erb'),
#  }

  # Allow people to talk to the KDC.
  #
  # Note 1: We set up the kadmin firewall rule on all KDC's, both master
  # and slave. This way, if a slave ever gets promoted to master its
  # kadmin service will be ready.
  base::iptables::rule {
    'hprop':
      description => $hprop_description,
      source      => $hprop_allowed,
      protocol    => 'tcp',
      port        => '754';
    'iprop':
      description => 'Allow slaves to access incremental Kerberos database propagation',
      source      => concat($slave_cidrs, $slave_ips),
      protocol    => 'tcp',
      port        => '2121';
    'kadmin':
      description => 'Kerberos administration from trusted subnets',
      source      => concat($slave_cidrs, $trusted_kadmin_cidrs),
      protocol    => 'tcp',
      port        => '749';
    'kdc':
      description => 'Kerberos KDC ticket requests',
      protocol    => [ 'tcp', 'udp' ],
      port        => $krb5_port;
    'kpasswd':
      description => 'Kerberos password changes',
      protocol    => 'udp',
      port        => '464';
    'ntp':
      description => 'Allow monitoring servers to check NTP status',
      source      => [ '171.67.217.112/28' ],
      protocol    => 'udp',
      port        => 123;
  }

  if ($hosts_allow_deny) {
    # Lock the systems down to only certain hosts.
    file {
      '/etc/hosts.allow':
        content => template('kdc/etc/hosts.allow.erb');
      '/etc/hosts.deny':
        content => template('kdc/etc/hosts.deny.erb');
    }
  }
}

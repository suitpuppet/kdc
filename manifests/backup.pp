#
# $extra_backups: extra unencrypted backups of Kerberos database beyond the
#   dailies
# $extra_backups_when: number of extra backups to keep. This should be an array
#  of hours suitable for the hour portion of a crontab job. For example,
#  [8, 12, 18] means to do the extra backups at 8:00 AM, 12:00 noon, and 6:00 PM.


# Set up encrypted backups.
class kdc::backup (
  String  $env               = undef,
  Boolean $cleartext_backups = true,
  Boolean $encrypted_backups = true,
  Boolean $copy_to_google    = false,
  Integer $backups_to_keep   = 14,
  #
  Boolean $history_cleartext_backups = true,
  Boolean $history_encrypted_backups = true,
  #
  Boolean        $extra_backups      = true,
  Array[Integer] $extra_backups_when = [10, 14, 18],
) {

  # Set up the backup directory. This directory holds both encrypted and
  # unencrypted backups.
  file { '/srv/backups/kdc':
    ensure => directory,
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  # Set up the cron job for kdc backups
  file { '/etc/cron.daily/kdc-backup':
    content => template('kdc/etc/cron.daily/kdc-backup.erb'),
    mode   => '0744',
    owner  => 'root',
    group  => 'root',
  }

  if ($extra_backups) {
    $extra_backup_directory = '/srv/backups/kdc-extra'
    file { $extra_backup_directory:
      ensure => directory,
      mode   => '0700',
      owner  => 'root',
      group  => 'root',
    }

    # Set up the cron job for EXTRA kdc backups
    file { '/etc/cron.d/kdc-backup-extra':
      ensure  => present,
      content => template('kdc/etc/cron.d/kdc-backup-extra.erb'),
    }
  } else {
    file { '/etc/cron.d/kdc-backup-extra':
      ensure  => absent,
    }
  }

  if ($encrypted_backups) {
    # Install the hprop backup password used to encrypt the hprop output.
    $wallet_name = "service/its-idg/kerberos-${kdc::env}/kdc-backup-password"
    base::wallet { $wallet_name:
      type  => 'file',
      path  => '/var/lib/heimdal-kdc/backup-password',
      mode  => '0600',
      owner => 'root',
      group => 'root',
    }

    # Make a symbolic link to this file.
    file { '/etc/heimdal-kdc/backup-password':
      ensure  => link,
      target  => '/var/lib/heimdal-kdc/backup-password',
      require => Base::Wallet[$wallet_name],
    }

    # If we are copying to Google Cloud Storage, we need the GCS
    # service account credentials. Uploading to GCS requires the
    # gcloud executable. gcloud is NOT installed by this module, instead,
    # the calling module must ensure that it is installed.
    if ($copy_to_google) {
      if ($env =~ /prod|uat/) {
        $gcs_wallet_name = 'service/its-idg/kdc/gcp-prod-kdc-backup-user'
      } else {
        $gcs_wallet_name = 'service/its-idg/kdc/gcp-stage-kdc-backup-user'
      }

      base::wallet { $gcs_wallet_name:
        path    => '/var/lib/heimdal-kdc/google-cloud-storage.key',
        type    => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
      }
    }
  }
}

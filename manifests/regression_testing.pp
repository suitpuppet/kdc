# Export the keytab for the regression testing service principal. Note
# that this principal has no power until it is added to the kadmind.acl
# file.  See also the $kadmind_acl_extras parameter in the kdc::config
# class.

class kdc::regression_testing (
  $env = undef,
)
{
  if (!$env) {
    fail("missing required 'env'")
  }

  # For details on the "setup-principal" command, log on to one of the
  # KDC's and type "man setup-principal". It is important that the principal
  # already exist or else the export will fail.
  $principal_name = "service/regression-testing-${env}"
  $keytab_file    = '/etc/heimdal-kdc/regression-testing.keytab'

  # $cmd_create = "setup-principal -n $principal_name -a replace -r -q"
  $cmd_export = "setup-principal -n $principal_name -a export -k $keytab_file"

  # Export the principal only if the keytab file does not exist.
  exec { "create $env regression testing principal":
    path    => '/usr/bin:/usr/sbin',
    command => "${cmd_export}",
    onlyif  => "test ! -f $keytab_file",
  }

}

# Things we do only on a "containerized" server.
#
#  * Map IP addresses to DNS entries.
#
class kdc::container (
  $address_to_hostname = {},
) {
  $mapping_file = '/etc/heimdal-kdc/ip-address-mapping'

  file { $mapping_file:
    ensure  => present,
    content => template('kdc/etc/heimdal-kdc/ip-address-mapping.erb'),
  }

}

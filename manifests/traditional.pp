# Things we do only on a "traditional" server.
#
#  * Setup up iptables rules.
#  * Make sure the kdc service keeps running.
#
class kdc::traditional (
  $env                           = undef,
  $master                        = false,
  $slave                         = false,
  $service_heimdal_kdc_enabled   = undef,
  $service_ipropd_master_enabled = undef,
  $service_ipropd_slave_enabled  = undef,
  $service_kpasswdd_enabled      = undef,
  $service_hpropd_enabled        = undef,
  $master_fqdn                   = undef,
  $master_ip                     = undef,
  $slaves                        = [],
  $slave_cidrs                   = [],
  $trusted_kadmin_cidrs          = [],
  $master_ipropd_keytab          = '/etc/krb5.keytab',
  $slave_ipropd_keytab           = '/etc/krb5.keytab',
  $extra_sshd_hosts_allow        = [],
  $extra_hpropd_cidrs            = [],
  $puppet_noop                   = true,
  $restrict_remctl               = true,
  $krb5_port                     = 88,
  $passive_monitors              = 'absent',
  #
  $master_time_gone_secs         = undef,
  $master_time_missing_secs      = undef,
  $slave_time_lost_secs          = undef,
  #
  $kdc_stats_acl                       = '/etc/remctl/acl/acs-linux',
  Array $kdc_list_root_principals_acls = ['/etc/remctl/acl/acs-linux',
                                          'princ:service/kdc-user-read@stanford.edu'],
  #
  $nagios_ips                    = [],
)
{
  if (!$env) {
    fail("missing required 'env'")
  }

  if (!$master_ip) {
    fail("missing required 'master_ip'")
  }

  # Set-up host-based firewall rules. We don't want the hosts.allow
  # and hosts.deny files managed in the DEV environment.
  if ($env == 'dev') {
    $hosts_allow_deny = false
  } else {
    $hosts_allow_deny = true
  }
  class {'kdc::firewall':
    master_ip                => $master_ip,
    master_fqdn              => $master_fqdn,
    krb5_port                => $krb5_port,
    slaves                   => $slaves,
    slave_cidrs              => $slave_cidrs,
    trusted_kadmin_cidrs     => $trusted_kadmin_cidrs,
    hosts_allow_deny         => $hosts_allow_deny,
    extra_sshd_hosts_allow   => $extra_sshd_hosts_allow,
    extra_hpropd_cidrs       => $extra_hpropd_cidrs,
  }

  # For traditional servers we get the master-key file from wallet.
  $mkey_file   = '/var/lib/heimdal-kdc/m-key'
  $wallet_name = "password/its-idg/kerberos-${env}/m-key"

  base::wallet { $wallet_name:
    path    => $mkey_file,
    type    => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }

  # Ensure the KDC is running.  We ideally would like to monitor all of the
  # other services that the init script may start, but that isn't set up
  # yet.  Don't require the server be running if the master key file doesn't
  # exist.
  if ($service_heimdal_kdc_enabled) {
    service { 'heimdal-kdc':
      ensure     => running,
      hasrestart => true,
      hasstatus  => false,
      status     => "pidof /usr/lib/heimdal-servers/kdc || test ! -f ${mkey_file}",
      require    => [
       Package['heimdal-kdc'],
       Base::Wallet[$wallet_name],
      ],
    }
  } else {
    service { 'heimdal-kdc':
      ensure     => stopped,
    }
  }

  # We only use /etc/default/heimdal-kdc on traditional servers, not on
  # containers.
  file { '/etc/default/heimdal-kdc':
    content => template('kdc/etc/default/heimdal-kdc.erb'),
  }

  # The hprop and iprop password file. This password is used for the service
  # principals "iprop/<slave-iprop-service-name>" and
  # principals "hprop/<slave-hprop-service-name>". This password should be
  # the same on all servers in the same environment.
  base::wallet { "password/its-idg/kerberos-${env}/prop-password":
    path    => '/var/lib/heimdal-kdc/prop-password',
    type    => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }

  # We setup rsyslog on traditional servers.
  include kdc::syslog

#  # Setup the wallet user (but only if we are allowing
#  # wallet servers to back up to us).
#  if ($wallet_server_backups) {
#    # Include the correct wallet user object
#    case $env {
#      'prod':    {
#        include service_users_and_groups::user::wallet::prod
#      }
#      'uat':    {
#        include service_users_and_groups::user::wallet::uat
#      }
#      'test':    {
#        include service_users_and_groups::user::wallet::test
#      }
#      default : {
#        fail("do not have wallet user object for environment '${env}'")
#      }
#    }
#
#    # Note that the service_users_and_groups::user::wallet::* class should
#    # create /srv/backups/wallet.
#    file { '/srv/backups/wallet/db':
#      ensure  => directory,
#      owner   => 'wallet',
#      group   => 'root',
#      require => File['/srv/backups/wallet'],
#    }
#    file { '/srv/backups/wallet/file-objects':
#      ensure  => directory,
#      owner   => 'wallet',
#      group   => 'root',
#      require => File['/srv/backups/wallet'],
#    }
#    file { '/srv/backups/wallet/password-objects':
#      ensure  => directory,
#      owner   => 'wallet',
#      group   => 'root',
#      require => File['/srv/backups/wallet'],
#    }
#  }

  # LATER: Use puppet_client for this in the future.
  ## Put Puppet in noop mode (if $puppet_noop set)
  #if ($puppet_noop) {
  #  include base::puppetclient::noop
  #}

  # Restrict who can connect to the remctl service.
  if ($restrict_remctl) {
    include kdc::traditional::remctl
  }

  # Create the regression testing principal (master only).
  if ($master) {
    class { 'kdc::regression_testing':
      env => $env,
    }
  }

  package { 'kdc-regression-testing':
    ensure => present
  }

  # The ensure for this class is set in the hiera YAML file.
  class { 'kdc::passive_monitors':
    env        => $env,
    krb5_port  => $krb5_port,
    host_name  => "kdc-master-${env}",
    nagios_ips => $nagios_ips,
  }

  # Setup the change_password keytab
  if ($master) {
    class { 'kdc::change_password':
      ensure => 'present',
    }
  } else {
    class { 'kdc::change_password':
      ensure => 'absent',
    }
  }

  # Make sure the krb5-sync keytab is installed. We can leave this on both
  # slaves and master (traditional only).
  $keytab_file = '/var/lib/heimdal-kdc/krb5-sync.keytab'
  exec { 'krb5-sync keytab file':
    path    => '/usr/bin:/usr/sbin',
    command => "kadmin -l ext_keytab --keytab=${keytab_file} service/krb5-sync",
    creates => $keytab_file,
  }

  # Add a cron job that ensures the kerberos server is running by checking
  # every 5 minutes. We do this because Puppet is locked, so Puppet cannot
  # ensure the service is up.
  file { '/etc/cron.d/krb5-kdc':
    source => 'puppet:///modules/kdc/etc/cron.d/krb5-kdc',
  }

  # For the master, add a remctl function that displays the file
  # /var/lib/heimdal-kdc/slaves-stats
  file { '/usr/sbin/cat-stats-slave':
    source => 'puppet:///modules/kdc/usr/sbin/cat-stats-slave',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }
  file { '/usr/sbin/cat-stats-master':
    source => 'puppet:///modules/kdc/usr/sbin/cat-stats-master',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }
  file { '/etc/remctl/conf.d/kdc-rep-stats':
    content => template('kdc/etc/remctl/conf.d/kdc-rep-stats.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }


  # Add remctl interface to kdc-stats
  file { '/etc/remctl/conf.d/kdc-stats':
    content => template('kdc/etc/remctl/conf.d/kdc-stats.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  # Add remctl interface to kdc-list-root-principals
  file { '/etc/remctl/conf.d/kdc-list-root-principals':
    content => template('kdc/etc/remctl/conf.d/kdc-list-root-principals.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

}

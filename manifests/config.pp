# Templated configuration for Heimdal Kerberos KDCs.  This handles all of the
# configuration that changes by environment, or that we might later want to
# change by environment, but does not install configuration files that only
# exist in one environment (like the slaves list for production KDCs)..
#
# This class does two things:
#  1. Installs a customized /etc/krb5.conf file.
#  2. Installs Heimdal KDC configuration files.
#
# IMPORTANT NOTE: Recall that the /etc/krb5.conf configuration file only
# affects kerberos when the server is running as a Kerberos CLIENT.
#
# Usage:
#
#     s_kdc::config { $::fqdn:
#       env        => 'prod',
#       realm      => 'stanford.edu',
#       master     => false,
#       slave      => false,
#       service_ad_sync_enabled => true,
#       sync_ad_realm           => 'MS.STANFORD.EDU',
#       sync_ad_admin_server    => 'ms.stanford.edu',
#       sync_ad_ldap_base       => 'ou=Accounts,dc=MS,dc=STANFORD,dc=EDU',
#     }
#
# $env: The environment, chosen from dev, test, uat, prod, qa, pre, or mit.
# Default: undef
#
# $realm: the Kerberos realm served by this KDC.
# Default: 'stanford.edu'
#
# $master: set to true to set up the server as an iprop master.
# Default: false
#
# $slave: set to true to set up the server as an iprop slave.
# Default: false
#
# ###
# ### krb5-sync settings (syncing passwords to an AD domain)
#
# $service_ad_sync_enabled: if set to true, password synchronization to the named Active
#   Directory realm will be configured and enabled.  When sync is set to
#   true sync_admin and sync_base should be defined as well to flesh out the
#   configuration.
# Default: undef
#
# $krb5_sync_timer: this parameter is used by the cron job that processes
#   the krb5-sync files in /var/spool/krb5-sync. Setting this parameter to
#   the integer N means that the krb-sync processor will run every N
#   minutes.
# Default: "30" (i.e., run every 30 minutes).
#
# ### end of krb5-sync settings
# ###
#
# $self_client: Set to true if you want the KDC's in this environment to
#   be Kerberos clients of themselves. Currently, only the test and uat are
#   self_clients (prod is also a self_client, but the prod environment
#   ignores the self_client parameter).
# Default: true
#
# $using_base_kerberos: Set this parameter if you will be using the
#   base::kerberos class, false otherwise. Typically, for traditional
#   servers this should be set to true and for containers set to false.
# Default: true
#
# $kadmind_acl_extras: extra lines to add to the bottom of the kadmind.acl
#   file. Each line will be added verbatim to the bottom of
#   /etc/heimdal-kdc/kadmind.acl. For more information on the format of
#   kadmind.acl, see the kadmind man page.
# Default: []
#
# $logging_kdc
# $logging_ipropd_slave
# $logging_ipropd_master
# $logging_default
#   These parameters will configure [logging] section of the file
#   /etc/heimdal-kdc/krb5.conf. For example:
#
#   logging_kdc         => 'FILE:/var/log/kdc',
#   loggin_ipropd_slave => 'STDERR',
#   logging_default     => 'FILE:/var/log/kdc-default',
#
#      [logging]
#        kdc = FILE:/var/log/kdc
#        ipropd-slave  = STDERR
#        ipropd-master = STDERR
#        default       = FILE:/var/log/kdc-default
#
#  Default: all default to SYSLOG:NOTICE
#
# $default_realm: in the rare case where the KDC server is NOT a client of
# the the production stanford.edu realm AND uses a different realm name,
# you can override the [libdefaults] default_realm setting here. So far the only
# KDC that needs this is kerberos-mit.
# Default: stanford.edu

class kdc::config (
  $ensure                  = 'present',
  $env                     = undef,
  $realm                   = 'stanford.edu',
  $master                  = false,
  $master_fqdn             = undef,
  $krb5_port               = '88',
  $slave                   = false,
  #
  $service_ad_sync_enabled = undef,
  $sync_ad_realm           = undef,
  $sync_ad_upn_realm       = undef,
  $sync_ad_admin_server    = undef,
  $sync_ad_ldap_base       = undef,
  $sync_ad_keytab          = undef,
  $sync_ad_base_instance   = undef,
  $sync_ad_queue_only      = false,
  #
  $krb5_sync_timer         = "30",
  $krb5_sync_silent        = false,
  $krb5_sync_process_dir   = '/var/spool/krb5-sync-process',
  $krb5_sync_ldap_master   = undef,
  #
  $self_client             = true,
  $kdcs                    = [],
  $slave_ipropd_keytab     = undef,
  $master_ipropd_keytab    = undef,
  $using_base_kerberos     = true,
  $kadmind_acl_extras      = [],
  $logging_kdc             = 'SYSLOG:NOTICE',
  $logging_ipropd_slave    = 'SYSLOG:NOTICE',
  $logging_ipropd_master   = 'SYSLOG:NOTICE',
  $logging_default         = 'SYSLOG:NOTICE',
  $default_realm           = 'stanford.edu',
  $extra_ip_to_hostnames   = {},
  $service_kadmind_enabled = undef,
  $password_quality_progam = undef,
  #
  $winuat_domain_controller = undef,
) {
  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be present or absent, not $ensure")
  }
  if (!$env) {
    fail("Mandatory parameter 'env' not set")
  }
  if !($env in [ 'sbx', 'dev', 'test', 'uat', 'qa', 'prod', 'pre', 'mit' ]) {
    fail("Unknown KDC environment ${env}")
  }
  if (!$master_fqdn) {
    fail("Mandatory parameter 'master_fqdn' not set")
  }

  # Install the customized krb5.conf file. If this is the production
  # environment or else the KDC is not a client of itself, use the
  # production settings for the stanford.edu realm.

  # Our standard is to use the "krb5auth-*" redirection for all
  # environments.

  # If the $kdcs parameter is empty, use the standard names.
  if (empty($kdcs)) {
    $kdcs_prod = [
      'krb5auth1.stanford.edu',
      'krb5auth2.stanford.edu',
      'krb5auth3.stanford.edu',
    ]

    $kdcs_self = [
      "krb5auth-${env}1.stanford.edu",
      "krb5auth-${env}2.stanford.edu",
      "krb5auth-${env}3.stanford.edu",
      "krb5auth-${env}4.stanford.edu",
    ]
  } else {
    $kdcs_self = $kdcs
  }

  if ($env == 'prod' or !$self_client) {
    $stanford_realm_is_production = true

    # Because we are using production version of stanford_realm, we
    # do not need these parameters:
    $master_kdc     = undef
    $admin_server   = undef
    $kpasswd_server = undef
    $kdcs_actual    = $kdcs_prod
  } else {
    $stanford_realm_is_production = false
    $master_kdc     = "kdc-master-${env}.stanford.edu"
    $admin_server   = "krb5-admin-${env}.stanford.edu"
    $kpasswd_server = "krb5-admin-${env}.stanford.edu"
    $kdcs_actual    = $kdcs_self
  }

  kdc::krb5_conf { '/etc/krb5.conf':
    env                           => $env,
    realm                         => $realm,
    default_realm                 => $default_realm,
    stanford_realm_is_production  => $stanford_realm_is_production,
    krb5_port                     => $krb5_port,
    kdcs                          => $kdcs_actual,
    master_kdc                    => $master_kdc,
    admin_server                  => $admin_server,
    kpasswd_server                => $kpasswd_server,
    service_ad_sync_enabled       => $service_ad_sync_enabled,
    sync_ad_realm                 => $sync_ad_realm,
    sync_ad_upn_realm             => $sync_ad_upn_realm,
    sync_ad_admin_server          => $sync_ad_admin_server,
    sync_ad_ldap_base             => $sync_ad_ldap_base,
    sync_ad_keytab                => $sync_ad_keytab,
    sync_ad_base_instance         => $sync_ad_base_instance,
    sync_ad_queue_only            => $sync_ad_queue_only,
    winuat_domain_controller      => $winuat_domain_controller,
  }

  # Install the KDC configuration for any of the Heimdal environments.
  # Production slaves don't actually need the kadmin.acl configuration, but it
  # doesn't hurt anything to have it in place.
  #
  # Note: since we don't use /etc/default/heimdal-kdc in the container
  # envoronment, that file is managed in traditional.pp.
  if ($env != 'mit') {
    $pwd_life = '1 year'

    # Set up /etc/heimdal-kdc/kdc.conf
    class { 'kdc::config::kdc_conf':
      env                     => $env,
      realm                   => $realm,
      krb5_port               => $krb5_port,
      service_ad_sync_enabled => $service_ad_sync_enabled,
      pwd_life                => '1 year',
      logging_kdc             => $logging_kdc,
      logging_ipropd_slave    => $logging_ipropd_slave,
      logging_ipropd_master   => $logging_ipropd_master,
      logging_default         => $logging_default,
      password_quality_progam => $password_quality_progam,
    }
  }

  # Set up kadmind.acl if we are running the kadmind service.
  if ($service_kadmind_enabled) {
    file { '/etc/heimdal-kdc/kadmind.acl':
      content => template('kdc/etc/heimdal-kdc/kadmind.acl.erb'),
      require => Package['heimdal-kdc'];
    }
  }

  # There is a special case we have to handle: if the master is doing
  # krb5-sync to an AD domain AND the master is NOT a self-client. In this
  # case, we can't use the usual /etc/krb5.conf since that points to
  # production. So, we have to make special krb5.conf that points to
  # itself and has the krb5-sync parameters. That is, this special
  # krb5.conf will be identical to /etc/krb5.conf except that its KDC's
  # point to itself.
  #
  # Note we don't need to do this for the prod enviroment.
  if ($service_ad_sync_enabled and !$self_client and ($env != 'prod')) {
    $krb5_sync_alternate_kr5b_conf = true
  } else {
    $krb5_sync_alternate_kr5b_conf = false
  }

  if ($krb5_sync_alternate_kr5b_conf) {
    $krb5_sync_master_kdc     = "kdc-master-${env}.stanford.edu"
    $krb5_sync_admin_server   = "krb5-admin-${env}.stanford.edu"
    $krb5_sync_kpasswd_server = "krb5-admin-${env}.stanford.edu"
    $krb5_sync_kdcs           = $kdcs_self

    kdc::krb5_conf { '/etc/heimdal-kdc/krb5-sync.conf':
      env                           => $env,
      realm                         => $realm,
      default_realm                 => $default_realm,
      stanford_realm_is_production  => false,
      kdcs                          => $krb5_sync_kdcs,
      master_kdc                    => $krb5_sync_master_kdc,
      admin_server                  => $krb5_sync_admin_server,
      kpasswd_server                => $krb5_sync_kpasswd_server,
      service_ad_sync_enabled       => $service_ad_sync_enabled,
      sync_ad_realm                 => $sync_ad_realm,
      sync_ad_upn_realm             => $sync_ad_upn_realm,
      sync_ad_admin_server          => $sync_ad_admin_server,
      sync_ad_ldap_base             => $sync_ad_ldap_base,
    }
  }

  # If we are _not_ doing password sync, disable the sync cron job.
  # Note that the krb5-sync.erb template file uses $krb5_sync_timer
  # parameter.
  if ($service_ad_sync_enabled) {
    file { $krb5_sync_process_dir:
      ensure => directory,
    }
    file { '/etc/cron.d/krb5-sync':
      ensure  => present,
      content => template('kdc/etc/cron.d/krb5-sync.erb'),
    }
  } else {
    file { '/etc/cron.d/krb5-sync': ensure => absent }
  }

  # REMOVE THIS FOR THE MOMENT AS WE MIGHT DO THIS A DIFFERENT WAY
  ## Add the file with extra ip address to hostname mappings.
  ## This is for containers that might not know what
  ## stanford.edu hostname their public IP address maps to.
  #$mapping_file = '/etc/heimdal-kdc/ip-address-mapping'
  #file { $mapping_file:
  #  ensure  => present,
  #  content => template('kdc/etc/heimdal-kdc/ip-address-mapping.erb'),
  #}

}

# We build our own kerberos packages, so we need to set up our APT
# preferences. Note that add a "require" to ALL Package resources so that
# we are sure that all the apt files are in place before loading any
# packages.

# $manage_debian_source: Add a file to /etc/apt/sources.list.d that points
#   to the Debian distribution and archive pointed to by
#   $debian_distribution and $debian_archive.  Remember that APT does not
#   like it when there are two source files pointing at the same
#   distribution, so in those cases where you have ALREADY added a list
#   file to /etc/apt/sources.d/ that covers this distribution, you will
#   want to set this parameter to false.
#   Default: false

# $manage_debian_preference: Add a /etc/apt/preferences.d file that
#   prefers $debian_distribution and $debian_archive as the source for
#   the Heimdal KDC packages.
#   Default: true

# $debian_distribution and $debian_archive: used by
#   $manage_debian_preference and $manage_debian_source, these values
#   will be used in the creation of the sources and preferences files.
#
#   If $manage_debian_source is true, the file source file
#   "/etc/apt/sources.list.d/$debian_distribution.list" will be created
#   using the values $debian_distribution and $debian_archive.
#
#   If $manage_debian_preference is true, the file source file
#   "/etc/apt/preferences.d/heimdal-kdc" will be created
#   using the values $debian_distribution and $debian_archive.
#
# $prefer_krb5_strength_backports: if set to true will add a preferences file
#   that says to prefer the backported version of the krb5-strength package.
# Default: true

# $manage_stanford_keyring: If this is set to true, then kdc::apt will
#   attempt to install stanford-keyring, otherwise it will not attempt to
#   install. For servers including the defaults class this parameter should
#   be set to false, while for containers (which typically do not include
#   the defaults class), this should be set to true.
#   Default: false

class kdc::apt (
  Boolean          $manage_debian_source      = false,
  Boolean          $manage_debian_preference  = true,
  #
  Optional[String] $debian_distribution       = undef,
  String           $debian_archive            = 'debian-stanford',
  Boolean          $prefer_krb5_strength_backports = false,
  #
  Boolean          $manage_stanford_keyring   = false,
){
  if ($manage_stanford_keyring) {
    # In case the stanford-keyring is already declared somewhere else, we
    # use ensure_resource.
    ensure_resource('package', 'stanford-keyring', {'ensure' => 'installed'})
  }

  ## SOURCES file
  if ($manage_debian_source) {
    su_apt::source { "kdc-${debian_distribution}":
      comment       => "Enable distribution $debian_distribution (kdc::apt)",
      archive       => $debian_archive,
      distributions => [$debian_distribution],
    }
  }

  ## PREFERENCES files
  if ($manage_debian_preference) {
    # Prefer our local KDC-specific repository whenever possible.
    file { '/etc/apt/preferences.d/heimdal-kdc':
      content => template('kdc/etc/apt/preferences.d/heimdal-kdc.erb'),
      notify  => Exec['kdc_aptitude_update'],
    }
  }

  if ($prefer_krb5_strength_backports) {
    # Prefer the backports version of krb5-strength.
    file { '/etc/apt/preferences.d/krb5-strength':
        content => template('kdc/etc/apt/preferences.d/krb5-strength.erb'),
        notify  => Exec['kdc_aptitude_update'],
    }
  }

  exec { 'kdc_aptitude_update':
    command     => 'aptitude update',
    path        => '/usr/bin:/usr/sbin:/bin',
    refreshonly => true,
  }

}

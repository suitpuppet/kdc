# Create the file /etc/heimdal-kdc/kdc.conf

# If customize is true, then the string defined by custom_kdc_name will be
# used in the [stanford] section rather than using $env.
#
# We support the 'destination' parameter to allow the generated file to be
# put in non-standard places.
class kdc::config::kdc_conf (
  $env                       = undef,
  $realm                     = 'stanford.edu',
  $destination               = '/etc/heimdal-kdc/kdc.conf',
  $customize                 = false,
  $custom_kdc_names          = [],
  $master_kdc                = undef,
  $krb5_port                 = 88,
  $service_ad_sync_enabled   = false,
  $pwd_life                  = '1 year',
  $logging_kdc               = 'SYSLOG:NOTICE',
  $logging_ipropd_slave      = 'SYSLOG:NOTICE',
  $logging_ipropd_master     = 'SYSLOG:NOTICE',
  $logging_default           = 'SYSLOG:NOTICE',
  $password_quality_progam   = '/usr/bin/heimdal-history',
  $dns_canonicalize_hostname = false,
) {

  ###
  ### The initial comment.
  $header_comment = @(HEADHEREDOC/L)
    # Heimdal KDC configuration.  -*- conf -*-
    #
    # This configuration is used by the heimdal-kdc and ipropd (master and slave) services.
    | HEADHEREDOC


  ### [kadmin] section.
  ###
  # This section looks like this:
  #
  # [kadmin]
  #     default_keys      = aes256-cts-hmac-sha1-96:pw-salt ...
  #     hook_libraries    = /usr/lib/x86_64-linux-gnu/krb5/plugins/kadm5_hook/sync.so
  #     password_lifetime = 1 year
  #
  # Note that the "hook_libraries" entry should only be present if we want
  # this KDC to do KDC-to-AD password syncing.

  # Cryptographic suites used in keys
  $crypt_suites = [
    'aes256-cts-hmac-sha1-96:pw-salt',
    'aes128-cts-hmac-sha1-96:pw-salt',
    'des3-cbc-sha1:pw-salt',
    'arcfour-hmac-md5:pw-salt',
  ]
  $crypt_suites_string = $crypt_suites.reduce |$memo, $value| { "${memo} ${value}" }

  # The [kadmin] section.
  if ($service_ad_sync_enabled) {
    $hook_libraries  = {
      'hook_libraries' =>  '/usr/lib/x86_64-linux-gnu/krb5/plugins/kadm5_hook/sync.so'
    }
  } else {
    $hook_libraries  = { }
  }

  $kadmin_section = {
    'default_keys'      => $crypt_suites_string,
    'password_lifetime' => $pwd_life,
  } + $hook_libraries

  ###
  ### The [kdc] section
  # This section looks like this:
  #
  # [kdc]
  #   log_file          = SYSLOG:NOTICE
  #   acl_file          = /etc/heimdal-kdc/kadmind.acl
  #   kdc_warn_pwexpire = 5d
  #
  $kdc_comment = @(KDCHEREDOC/L)
   log_file
     Where changes performed to the database are logged.
   acl_file
     Where ACLs for the database are kept.
   ports
     The list of ports the KDC listens to.
     NOTE. we omit this parameter as it is set in
     the /etd/defaults/heimdal-kdc file.
   kdc_warn_pwexpire:
     The time before expiration that the user should be warned
     that her password is about to expire.
   | KDCHEREDOC

   $kdc_section = {
     'COMMENT'           => $kdc_comment,
     'log_file'          => 'SYSLOG:NOTICE',
     'acl_file'          => '/etc/heimdal-kdc/kadmind.acl',
     'kdc_warn_pwexpire' => '5d',
   }

  ###
  ### The [libdefaults] section
  # This section looks like this:
  #
  # [libdefaults]
  #   default_realm      = stanford.edu
  #   dns_lookup_kdc     = false
  #   dns_lookup_realm   = false
  #   allow_weak_crypto  = true
  if ($dns_canonicalize_hostname) {
    $dns_canonicalize_hostname_str = 'true'
  } else {
    $dns_canonicalize_hostname_str = 'false'
  }
  $libdefaults_section = {
    'default_realm'             =>  $realm,
    'dns_lookup_kdc'            => 'false',
    'dns_lookup_realm'          => 'false',
    'allow_weak_crypto'         => 'true',
    'dns_canonicalize_hostname' => $dns_canonicalize_hostname_str,
  }

  ###
  ### The [password_quality] section
  # This section looks like this:
  #
  # [password_quality]
  #   policies         = external-check
  #   external_program = /usr/bin/heimdal-history
  $pq_comment = @(PQHEREDOC/L)
   The program /usr/bin/heimdal-history checks the password's complexity by
   calling the external program heimdal-strength. This program is configured
   in /etc/krb5.conf under krb5-strength.
  | PQHEREDOC
  $password_quality_section = {
    'COMMENT'          => $pq_comment,
    'policies'         => 'external-check',
    'external_program' => $password_quality_progam,
  }

  ###
  ### The [realms] section
  # We override the stanford.edu realm here:
  if ($master_kdc) {
    $master_kdc_real = $master_kdc
  } else {
    $master_kdc_real = "kdc-master-${env}.stanford.edu"
  }
  if ($customize) {
    $kdc_names = $custom_kdc_names
  } elsif ($env =~ /sbx|dev|test|uat|qa/) {
    $kdc_names = [ $master_kdc_real ]
  } else {
    $kdc_names = [
      'krb5auth2.stanford.edu',
      'krb5auth1.stanford.edu',
      'krb5auth3.stanford.edu',
    ]
  }
  $kdcs_mapped = $kdc_names.map |$k| { ['kdc', "${k}:${krb5_port}"] }

  $realms_additional = {
    'stanford.edu' =>
      $kdcs_mapped + [
        ['master_kdc',     "${master_kdc_real}:${krb5_port}"],
        ['admin_server',   $master_kdc_real],
        ['kpasswd_server', $master_kdc_real],
        ['default_domain', $realm],
        ['kadmind_port',   '749']
      ]
  }

  ###
  ### The [logging] section
  $logging_section = {
    'kdc'           => $logging_kdc,
    'ipropd-slave'  => $logging_ipropd_slave,
    'ipropd-master' => $logging_ipropd_master,
    'default'       => $logging_default,
  }

  krb5conf { $destination:
    header_comment     => $header_comment,
    libdefaults        => $libdefaults_section,
    other_sections     => {
      'kadmin'           => $kadmin_section,
      'kdc'              => $kdc_section,
      'password_quality' => $password_quality_section,
    },
    realms_additional   => $realms_additional,
    logging             => $logging_section,
    other_section_files => ['kdc/etc/heimdal-kdc/capaths'],
  }
}

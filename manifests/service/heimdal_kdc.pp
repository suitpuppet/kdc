class kdc::service::heimdal_kdc {

  if ($kdc::hosting_model == 'container') {
    kdc::supervisor::module { 'heimdal-kdc':
      ensure => present
    }
  }

}

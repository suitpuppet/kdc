class kdc::service::ipropd_slave(
  $ensure     = undef,
  $nagios_ips = [],
){

  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be one of 'present' or 'absent'")
  }

  if ($kdc::hosting_model == 'container') {
    kdc::supervisor::module { 'ipropd-slave':
      ensure => $ensure
    }
  }

  # Set up replication monitoring. This is only for traditional hosting
  # model; containers are on their own for setting this monitor up.
  if ($kdc::hosting_model == 'traditional') {
    file { '/etc/cron.d/monitor-replication-nagios':
      ensure  => $ensure,
      content => template('kdc/etc/cron.d/monitor-replication-nagios.erb'),
    }
  }
}

class kdc::service::kadmind {

  # Symlink the kadmind ACL file to somewhere more useful.
  file { '/var/lib/heimdal-kdc/kadmind.acl':
    ensure  => link,
    target  => '/etc/heimdal-kdc/kadmind.acl',
    require => Package['heimdal-kdc'],
  }

  # Enable the kadmind service via xinetd.
  base::xinetd::config { 'kerberos-adm':
    server      => '/usr/lib/heimdal-servers/kadmind',
    server_args => '--config-file=/etc/heimdal-kdc/kdc.conf',
    description => 'Server for administrative access to the KDC database',
    cps         => '250 5',
    log_type    => 'auth',
  }

  # Filter out additional syslog noise from Kerberos administration.
  file { '/etc/filter-syslog/kadmin':
    source => 'puppet:///modules/kdc/etc/filter-syslog/kadmin',
  }

}


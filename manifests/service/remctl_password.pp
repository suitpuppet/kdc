# The remctl-password service.
#
# This is remctl service that allows password changes.

class kdc::service::remctl_password {

  file {
    '/etc/remctl/password':
      ensure => directory;
    '/etc/remctl/password/remctl.conf':
      source => 'puppet:///modules/kdc/etc/remctl/password/remctl.conf';
  }

  base::xinetd::config { 'remctl-password':
    server      => '/usr/sbin/remctld',
    server_args => '-f /etc/remctl/password/remctl.conf -k /etc/remctl/password/keytab',
    description => 'remctl server for user password change',
    server_type => 'UNLISTED',
    port        => '4443',
    cps         => '100 5',
    env         => 'PATH=/sbin:bin:/usr/sbin:/usr/bin',
  }

  if ($kdc::hosting_model == 'traditional') {
    base::iptables::rule { 'remctl-password':
      description => 'Allow password change remctl from Stanford subnets',
      source      => [ '10.0.0.0/8',
                       '68.65.160.0/20',
                       '128.12.0.0/16',
                       '134.79.0.0/16',
                       '171.64.0.0/14',
                       '172.16.0.0/12',
                       '192.168.0.0/16',
                       '204.63.224.0/21',
                       '100.20.76.164/32',
                       '34.215.26.127/32',
                       '54.190.192.83/32',
                       '35.86.9.27/32',
                       '35.247.50.152/32',
                       '54.189.188.2/32'],
      protocol    => 'tcp',
      port        => 4443,
    }
  }
  
}

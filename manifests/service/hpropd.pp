class kdc::service::hpropd (
  $slave_ipropd_keytab = '/etc/krb5.keytab',
) {

  base::xinetd::config { 'hprop':
    server      => '/usr/sbin/hpropd',
    server_args => "--database=/var/lib/heimdal-kdc/heimdal --keytab=$slave_ipropd_keytab",
    description => 'Full KDC database propagation server for Heimdal',
    log_type    => 'auth',
  }

}

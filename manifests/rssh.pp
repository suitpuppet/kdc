# rssh configuration to allow rsync for wallet user for backing up wallet
# objects from lsdb.
#
# Note that we use the text_line resource from PuppetLabs stdlib module.
class kdc::rssh {

  package { 'rssh': ensure => installed }
  file_line { 'rsync_only_wallet':
      ensure  => present,
      line    => 'user=wallet:011:100000:  # rsync only for wallet, with no chroot',
      path    => '/etc/rssh.conf',
      require => Package['rssh'],
  }

  # Filter out rssh lines used by lsdb connecting to the server.
  file { '/etc/filter-syslog/rssh':
    source => 'puppet:///modules/kdc/etc/filter-syslog/rssh';
  }

}

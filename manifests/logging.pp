class kdc::syslog inherits base::syslog {
  Base::Syslog::Config::Rsyslog['/etc/rsyslog.conf'] {
    use_syslog_conf => false,
    use_v5          => false,
  }

  file { '/var/log/auth':
    ensure => file,
    mode   => '0640',
    owner  => 'root',
    group  => '0',
  }

}

class kdc::principal_exists (
  Enum['present', 'absent'] $ensure,
  $remctl_acls = [],
) {

  file { '/etc/remctl/conf.d/principal-exists':
    ensure  => $ensure,
    content => template('kdc/etc/remctl/conf.d/principal-exists.erb'),
  }

}

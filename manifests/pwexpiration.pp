# Install the credentials and scripts neccesary to run the password
# expiration syncing from Kerberos to LDAP
#
# This class does NOT install the package stanford-kdc-tools which contains
# the necessary passsword expiration sync script. You will need to install
# stanford-kdc-tools elsewhere.


# If $keytab_k5start_service is set to "true" then a systemd service is
# installed that creates a k5start refreshe service for the
# pwexpiration.keytab keytab file. This is not needed for the password
# expiration cron job, but is useful for any other service that might need
# the credentials.

class kdc::pwexpiration (
  Enum['present', 'absent']
          $ensure                 = 'absent',
          $env                    = undef,
          $ldap_master            = undef,
          $delay_seconds          = 20,
  Boolean $use_lock_file          = true,
  Boolean $keytab_k5start_service = false,
) {

  if (!$ensure) {
    fail("Mandatory parameter 'ensure' not set")
  }

  if ($ensure == 'present') {
    if (!$env) {
      fail("Mandatory parameter 'env' not set")
    }

    if (!$ldap_master) {
      fail("Mandatory parameter 'ldap_master' not set")
    }

    if !($env in [ 'sbx', 'dev', 'test', 'uat', 'qa', 'prod', 'pre', 'mit' ]) {
      fail("Unknown KDC environment ${env}")
    }
  }

  # The keytab.
  $wallet_name = "service/kerberos-expiration-$env"

  # Note that this wallet object is going on a KDC which has heimdal
  # clients, thus we need to set the heimdal parameter to true.
  base::wallet { $wallet_name:
    ensure  => $ensure,
    path    => '/etc/pwexpiration.keytab',
    type    => 'keytab',
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    heimdal => true,
  }

  # The cron job
  file { '/etc/cron.d/password-expiration-sync':
    ensure  => $ensure,
    content => template('kdc/etc/cron.d/password-expiration-sync.erb'),
  }

  # We need a production krb5.conf file in case we are running on a
  # non-production server.
  file { '/etc/krb5_prod.conf':
    ensure => $ensure,
    source => 'puppet:///modules/kdc/etc/krb5_prod.conf',
  }

  # We need a wrapper script to use the special production krb5.conf file.
  file { '/usr/sbin/password-expiration-wrapper':
    ensure => $ensure,
    content => template('kdc/usr/sbin/password-expiration-wrapper.erb'),
    mode   => '0500',
  }

  # Set up newsyslog to rotate the logs. This assumes we are on a
  # "traditional" server.
  base::newsyslog::config { 'password_expiration':
    ensure    => $ensure,
    frequency => 'daily',
    directory => '/var/log',
    logs      => [ 'password-expiration.log' ],
    analyze   => '/usr/bin/filter-syslog',
    save_num  => 30,
  }

  file { '/etc/filter-syslog/password-expiration':
    ensure => $ensure,
    source => 'puppet:///modules/kdc/etc/filter-syslog/password-expiration',
  }

  if (($ensure == 'present') and $keytab_k5start_service) {
    # PRESENT
    # Make sure we start before multi-user systemd service so
    # that the service is actually enabled.
    systemd_k5start { 'k5start-pwexpiration':
      keytab       => '/etc/pwexpiration.keytab',
      ticket_file  => '/run/pwexpiration.tkt',
      start_before => 'multi-user.target',
    }

    # We have to define the service ourselves:
    service { 'k5start-pwexpiration':
      ensure  => 'running',
      enable  => true,
      require => Systemd_K5start['k5start-pwexpiration'],
    }
  } else {
    # ABSENT
    systemd_k5start { 'k5start-pwexpiration':
      ensure => absent,
    }

    service { 'k5start-pwexpiration':
      ensure => false,
    }
  }
}

class kdc::passive_monitors (
         $ensure                = 'absent',
         $env                   = undef,
         $krb5_port             = 88,
         $nagios_ips            = [],
         $basic                 = true,
         $simple                = true,
         $dictionary            = true,
         $history               = true,
         $ad_krb5_sync          = true,
         $pw_exp_sync           = false,
  String $host_name             = undef,
         $sync_spool_dir_length_incoming = 200,
         $sync_spool_dir_length_process  =  30,
) {

  $testing_principal = "service/regression-testing-${env}"

  file { '/etc/cron.d/kdc-passive-monitors':
    ensure => $ensure,
    content => template('kdc/etc/cron.d/kdc-passive-monitors.erb'),
  }
}

# Configuration for Heimdal KDC slaves.
#
# This class applies to all production Heimdal KDC slaves, from whatever
# environment: production, UAT, or test.

class kdc::slave (
  $slave_ipropd_keytab = '/etc/krb5.keytab',
  $service_hpropd      = false,
)
{
  if ($service_hpropd) {
    base::xinetd::config { 'hprop':
      server      => '/usr/sbin/hpropd',
      server_args => "--database=/var/lib/heimdal-kdc/heimdal --keytab=$slave_ipropd_keytab",
      description => 'Full KDC database propagation server for Heimdal',
      log_type    => 'auth',
    }
  }

}

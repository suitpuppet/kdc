class kdc::master (
  Enum['present', 'absent']
                   $ensure                  = undef,
                   $slaves                  = [],
  Boolean          $heimdal_history_cleanup = false,
  Boolean          $service_ad_sync_enabled = true,
  Integer          $krb5_sync_purge_days    = 14,
  Optional[String] $krb5_sync_dir           = undef,
  Optional[String] $krb5_sync_process_dir   = undef,
){
  package {
    'krb5-sync-plugin-heimdal':   ensure => $ensure;
    'krb5-sync-tools':            ensure => $ensure;
    'stanford-kdc-password-expiration':
                                  ensure => $ensure;
    'stanford-server-kdc-master': ensure => $ensure;
  }

  # We only install the krb5-sync-purge cron job if this is
  # master AND $service_ad_sync_enabled is true.
  if ($service_ad_sync_enabled and ($ensure == 'present')) {
    $kerb5_sync_ensure = 'present'
  } else {
    $kerb5_sync_ensure = 'absent'
  }
  # Purge queued password changes older than $krb5_sync_purge_days days.
  file { '/etc/cron.daily/krb5-sync-purge':
    ensure  => $kerb5_sync_ensure,
    content => template('kdc/etc/cron.daily/krb5-sync-purge.erb'),
    mode    => '0755',
  }

  # Configure the slaves we allow to connect to the ipropd-master
  # service. We allow this to persist on both master and slave.
  file {
    '/etc/heimdal-kdc/slaves':
      content => template('kdc/etc/heimdal-kdc/slaves.erb'),
      require => Package['heimdal-kdc'];
    '/var/lib/heimdal-kdc/slaves':
      ensure  => link,
      target  => '/etc/heimdal-kdc/slaves',
      require => Package['heimdal-kdc'];
  }

  # Install a cron job to change the password of our test principal
  # every several minutes allowing us to monitor replication at the
  # application level.
  file { '/etc/cron.d/kdc-cpw-test-principal':
    ensure => $ensure,
    source => 'puppet:///modules/kdc/etc/cron.d/kdc-cpw-test-principal',
  }

  include kdc::pwexpiration

  if ($heimdal_history_cleanup) {
    file { '/etc/cron.daily/heimdal-history-clean':
      ensure => present,
      source => 'puppet:///modules/kdc/etc/cron.daily/heimdal-history-clean',
      mode   => '0755',
    }
  } else {
    file { '/etc/cron.daily/heimdal-history-clean':
      ensure => absent,
    }
  }

  # The Heidmal password history database files must be owned by user
  # "_history".
  if ($ensure == 'present') {
    file { '/var/lib/heimdal-history/history.db':
      ensure => present,
      owner  => '_history',
      group  => '_history',
    }
    file { '/var/lib/heimdal-history/lengths.db':
      ensure => present,
      owner  => '_history',
      group  => '_history',
    }
  }

  $keytab_file_password = '/etc/remctl/password/keytab'
  if ($ensure == 'present') {
    # Make sure the kadmin password.
    exec { 'kadmin password keytab file':
      path    => '/usr/bin:/usr/sbin',
      command => "kadmin -l ext_keytab --keytab=${keytab_file_password} kadmin/changepw",
      creates => $keytab_file_password,
    }
  } else {
    file { $keytab_file_password:
      ensure => absent
    }
  }
}

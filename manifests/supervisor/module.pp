#
# Currently defined supervisord modules:
#  - heimdal-kdc  (the KDC service)
#  - ipropd-slave (replication service for a slave)
#
define kdc::supervisor::module (
  $ensure = present
){

  file { "/etc/supervisor/conf.d/${name}.conf":
    ensure => $ensure,
    source => "puppet:///modules/kdc/etc/supervisor/conf.d/${name}.conf",
  }

}

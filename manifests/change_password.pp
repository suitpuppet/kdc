# This sets up the keytab for the change password backe end (the one used
# in the remctl interface).
#
# See also /usr/sbin/password-backend which is in the
# stanford-server-kdc-master Debian package.
#

class kdc::change_password (
  $ensure = undef,
) {

  if (($ensure != 'present') and ($ensure != 'absent')) {
    crit("ensure parameter must be one of 'present' or 'absent'")
  }

  $principal   = 'service/backend-cpw'
  $keytab_file = '/var/lib/heimdal-kdc/backend-cpw.keytab'

  if ($ensure == 'present') {
    # Step 1. Make sure the keytab is exported.
    exec { 'export-service/backend-cpw':
      path    => '/usr/bin',
      command => "kadmin -l ext_keytab -k $keytab_file $principal",
      creates => $keytab_file,
    }

    # Step 2. Add a line to the ACL file. You must do this YOURSELF.
  } else {
    file { $keytab_file:
      ensure => absent
    }
  }

}

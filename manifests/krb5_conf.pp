# A define that creates a krb5.conf file.
#
# The $name parameter is where the file will be put.
#
# In the "stanford.edu" section of [realms], by default the production
# settings will appear:
#
#   [realms]
#       stanford.edu = {
#           kdc            = krb5auth1.stanford.edu:88
#           kdc            = krb5auth2.stanford.edu:88
#           kdc            = krb5auth3.stanford.edu:88
#           master_kdc     = kdc-master.stanford.edu:88
#           admin_server   = krb5-admin.stanford.edu
#           kpasswd_server = krb5-admin.stanford.edu
#           default_domain = stanford.edu
#           kadmind_port   = 749
#       }
#
# If you want to override these using these parameters, set the parameter
# $stanford_realm_is_production to false and then use the following
# parameters:
#
# $kdcs: If this array is not empty, use this set of server names
#   for the "kdc" entries in the realm. If the array is empty, use the
#   the normal production KDC list.
#
# Example:
#  kdcs => ['kerberos-qa2.stanford.edu', 'kerberos-qa1.stanford.edu'],
#
# will result in
#
# [realms]
#   stanford.edu = {
#     kdc            = kerberos-qa2.stanford.edu:88
#     kdc            = kerberos-qa1.stanford.edu:88
#
#
define kdc::krb5_conf (
  $env                             = undef,
  $realm                           = 'stanford.edu',
  $default_realm                   = 'stanford.edu',
  $stanford_realm_is_production    = true,
  $krb5_port                       = '88',
  $kdcs                            = [],
  $master_kdc                      = 'kdc-master.stanford.edu',
  $admin_server                    = 'krb5-admin.stanford.edu',
  $kpasswd_server                  = 'krb5-admin.stanford.edu',
  $passwd_change_passwd_file       = '/afs/ir.stanford.edu/service/etc/passwd.all',
  $passwd_change_server            = 'password-change.stanford.edu',
  $passwd_change_service_principal = 'service/password-change@stanford.edu',
  $service_ad_sync_enabled         = false,
  $sync_ad_realm                   = undef,
  $sync_ad_upn_realm               = undef,
  $sync_ad_admin_server            = undef,
  $sync_ad_ldap_base               = undef,
  $sync_ad_keytab                  = '/etc/krb5kdc/ad-keytab',
  $sync_ad_principal               = undef,
  $sync_ad_base_instance           = undef,
  $sync_ad_queue_only              = false,
  #
  $winuat_domain_controller        = 'winuatdc1.winuat.stanford.edu',
) {

  if (!$sync_ad_principal) {
    $sync_ad_principal_real = "service/krb5-sync@${realm}"
  } else {
    $sync_ad_principal_real = $sync_ad_principal
  }

  #file { $name:
  #  content => template('kdc/etc/krb5.conf.erb'),
  #}

  # Define the appdefaults string. This is where the krb5-sync
  # section is defined.
  $appdefaults = template('kdc/etc/appdefaults.erb')

  $kdcs_mapped = $kdcs.map |$k| { ['kdc', "${k}:${krb5_port}"] }

  # We override the stanford.edu and WINUAT realms here:
  krb5conf { $name:
    appdefaults_string => $appdefaults,
    realms_additional  => {
      'stanford.edu' =>
        $kdcs_mapped + [
          ['master_kdc',     "${master_kdc}:${krb5_port}"],
          ['admin_server',   $admin_server],
          ['kpasswd_server', $kpasswd_server],
          ['default_domain', $default_realm],
          ['kadmind_port',   '749']
        ],
      'WINUAT.STANFORD.EDU' =>
        [
          ['kdc',            "${winuat_domain_controller}:88"],
          ['kpasswd_server', $winuat_domain_controller],
        ],
    }
  }
}

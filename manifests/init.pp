# Configuration for a Heimdal Kerberos KDC. This class does not attempt
# to make any general system changes, rather, it focusses on getting the
# Heimdal KDC and ipropd services running.
#
#
# QUICK START
# -----------
#
# To set up a typical master on a traditional server:
#
# class { 'kdc':
#   master                  => true,
#   env                     => 'test',
#   hosting_model           => 'traditional',
# }
#
#
# SERVICES
# --------
# There are several services that a KDC can run:
#
#  (1) The KDC service.
#  (2) The "master" ipropd service (run only on masters).
#  (3) The "slave"  ipropd service (run only on slaves)
#  (4) The remote kadmind service (run only on masters)
#  (5) The kpasswdd password change service (run only on masters)
#  (6) The hpropd service (run only on slaves)
#  (7) The "remctl-password" service: an xinetd-managed service allowing
#      password changed via remctl (run on only masters).
#  (8) The AD sync service.
#
# Every KDC server should be runing the KDC service (1).
#
# Note that a server can be BOTH a master and a slave. However, we currently
# do not have any servers running as both master and slave.
#
# These services are enabled or disabled automatically based on whether the
# KDC is a master or slave. To override this, see the ...
#
#
# HOSTING MODEL
# -------------
# This class supports two hosting models: the "container" model (i.e.,
# Docker), and the "traditional" server model. Depending on which model
# is selected, some files and services will be managed differently.
#
# To select which model to use, set the $hosting_model parameter to either
# "container" or "traditional". To see the extra services and files
# managed by the traditional hosting model, see the file traditional.pp
#
#
# PARAMETERS
# ----------
#
# $master
#   Set $master to true to make server act as a KDC master.
# Default: false
#
# $slave
#   Set $slave to true to make server act as a KDC slave.
# Default: false
#
# $hosting_model
#   Indicates what sort of hosting model we should use. Should be one of
#   "container" or "traditional".
# Default: traditional.
#
# $env
#   The environment. Must be one:
#     * prod
#     * uat
#     * test
#     * dev
#     * sbx
#     * qa
# Default: undef
#
# $krb5_port
#   Normally, the KDC service listens on port 88 to both TCP and UDP
#   traffic. To change the port number, use this parameter. Note that this
#   will affect the iptables.
#   Example:
#     krb5_port => 588
#   Default: 88
#
#
# Turning on and off various services
# ===================================
#
# Normally, all the services are enabled or disabled autimatically based
# on whether the KDC is a master or slave. However, if you want to override
# any of these services, use one of the $service_* parameters:
#
#  $service_heimdal_kdc
#  $service_ipropd_master
#  $service_ipropd_slave
#  $service_kadmind
#  $service_kpasswdd
#  $service_hpropd
#  $service_remctl_password
#  $service_ad_sync
#
# The default value of these parametes is "undef". To force a service to be enabled,
# set the parameter to "enabled"; to force a service to be disabled, set the parameter to
# "disabled".
#
#
# Password synchronization
# ===================================
#
# If the KDC is a master (or $service_ad_sync is set to "enable", then
# the kerberos-to-Active Directory account sync is enabled.
#
# In this class, these also need to be set:
#  $sync_ad_realm
#  $sync_ad_admin_server
#  $sync_ad_ldap_base
#
#
# Other parameters
# ===================================
#
# $master_fqdn
#   The full-qualified domain name of the master KDC. This is used in a
#   couple places, most notably in the ipropd-slave settings to tell the a
#   slave where to replicate.
# Example: kerberos-test.stanford.edu
# Default: undef
#
# $slaves
#   This is an ARRAY of slaves that the master allows to replicate its
#   database to via ipropd. Example:
#     [
#       'kerberos2.stanford.edu',
#       'kerberos3.stanford.edu',
#       'kerberos-liv.stanford.edu',
#       'kdc-uat-2',
#       'kdc-uat-3',
#     ]
#
#  These names will be added to the /etc/heimdal/slaves file.
#
#  Note: the slaves do NOT have to be fully-qualified, rather, the names
#        should match the 'iprop' principal name used by the slave as
#        credentials. However, _do_ use the fully-qualified name for
#        those slaves that use the fully-qualified name in their iprop
#        principal.
#  Default: the empty array.
#
# $slave_cidrs
#   An array of CIDR ranges that contain all the slaves that
#   need to replicate from the master. These should all be
#   iptables-comptible IP address strings. Anything not included in this
#   list will NOT be allowed to connect to the ipropd service on the master.
# Default: []
#
# $local_backup
#   If set to true will make a backup of the kerberos
#   database to the local disk in /srv/backups/kdc/. Not really necessary
#   for dev or test environments.
# Default: true
#
# $self_client
#   If set to true, then this KDC should act as a client of itself, that
#   is, when it acts as a Kerberos client is should use itself as the
#   KDC. If set to false will use Stanford's production KDC as its KDC.
#   Currently, these are the only environments acting as self-clients:
#     * uat
#     * test
#     * mit (??)
# Default: true
#
# $use_filtersyslog: Set this parameter to true to include the
#   base::filtersyslog class and install KDC-specific filter syslog
#   configuration files.
# Default: true
#
# $extra_sshd_hosts_allow: An array of hosts.allow compatible addresses
#   that should be allowed to ssh into the server. See the "PATTERNS"
#   section of hosts.allow's man page for details on what is allowed.
# Default: []
#
# $wallet_server_backups: An array of hosts.allow  compatible addresses
#   that are wallet servers and should be allowed to ssh into the server
#   in order to backup wallet objects.
#
# $using_base_kerberos: Set this parameter if you will be using the
#   base::kerberos class, false otherwise. Typically, for traditional
#   servers this should be set to true and for containers set to false.
# Default: true
#
# $kadmind_acl_extras: extra lines to add to the bottom of the kadmind.acl
#   file. Each line will be added verbatim to the bottom of
#   /etc/heimdal-kdc/kadmind.acl. For more information on the format of
#   kadmind.acl, see the kadmind man page.
# Default: []
#
# $logging_kdc
# $logging_ipropd_slave
# $logging_ipropd_master
# $logging_default
# See config.pp for details on these four parameters.
#

class kdc(
  $master                  = false,
  $slave                   = false,
  #
  $service_heimdal_kdc     = undef,
  $service_ipropd_master   = undef,
  $service_ipropd_slave    = undef,
  $service_kadmind         = undef,
  $service_kpasswdd        = undef,
  $service_hpropd          = undef,
  $service_remctl_password = undef,
  $service_ad_sync         = undef,
  #
  $hosting_model     = 'traditional',
  $env               = undef,
  $master_fqdn       = undef,
  $master_ip         = undef,
  $krb5_port         = '88',
  $slave_cidrs       = [],
  $slaves            = [],
  #
  $local_backup            = true,
  $copy_to_google          = false,
  Integer $backups_to_keep = 14,
  #
  $realm             = 'stanford.edu',
  $self_client       = true,
  $kdcs              = [],
  $use_filtersyslog  = true,
  $trusted_kadmin_cidrs =
    [
      '171.64.11.53',
      '171.64.18.0/23',
      '171.67.218.0/28',
      '171.67.218.32/28',
      '171.67.225.128/26',
      '204.63.227.88/29',
      '172.20.196.23',
    ],
  $extra_sshd_hosts_allow = [],
#  $wallet_server_backups  = [],
  $extra_hpropd_cidrs     = [],
  $slave_ipropd_keytab   = '/etc/krb5.keytab',
  $master_ipropd_keytab  = '/etc/krb5.keytab',
  $using_base_kerberos   = true,
  $kadmind_acl_extras    = [],
  $logging_kdc           = 'SYSLOG:NOTICE',
  $logging_ipropd_slave  = 'SYSLOG:NOTICE',
  $logging_ipropd_master = 'SYSLOG:NOTICE',
  $logging_default       = 'SYSLOG:NOTICE',
  #
  $sync_ad_realm         = undef,
  $sync_ad_upn_realm     = undef,
  $sync_ad_admin_server  = undef,
  $sync_ad_ldap_base     = undef,
  $sync_ad_keytab        = undef,
  $sync_ad_base_instance = undef,
  $sync_ad_queue_only    = true,
  #
  $master_time_gone_secs    = '300',
  $master_time_missing_secs = '300',
  $slave_time_lost_secs     = '300',
  #
  $krb5_sync_timer          = '30',
  $krb5_sync_silent         = false,
  $krb5_sync_dir            = '/var/spool/krb5-sync',
  $krb5_sync_process_dir    = '/var/spool/krb5-sync-process',
  $krb5_sync_purge_days     = 14,
  $krb5_sync_ldap_master    = undef,
  #
  $password_quality_progam  = undef,
  $extra_ip_to_hostnames    = {},
  #
  $nagios_ips               = [],
  #
  $winuat_domain_controller = undef,
) {

  if !($hosting_model in [ 'traditional', 'container' ]) {
    fail("Unknown hosting model ${hosting_model}")
  }

  if (!$master_fqdn) {
    fail("missing required 'master_fqdn'")
  }

  ##########################################################
  # Decide whether to enable various services based on KDC type
  # (master or slave) and the override parameters ($service_*).
  #
  # $service_heimdal_kdc_enabled
  # $service_ipropd_master_enabled
  # $service_ipropd_slave_enabled
  # $service_hpropd_enabled
  # $service_kadmind_enabled
  # $service_kpasswdd_enabled
  # $service_remctl_password_enabled
  # $service_ad_sync_enabled

  # All KDC's get the heimdal_kdc service.
  if (($service_heimdal_kdc == undef) or ($service_heimdal_kdc =~ /^enable/)) {
    $service_heimdal_kdc_enabled = true
  } else {
    $service_heimdal_kdc_enabled = false
  }

  # Slaves get the ipropd_slave service.
  if ($service_ipropd_slave == undef) {
    if ($slave) {
      $service_ipropd_slave_enabled = true
    } else {
      $service_ipropd_slave_enabled = false
    }
  } elsif ($service_ipropd_slave =~ /^disable/) {
    $service_ipropd_slave_enabled = false
  } elsif ($service_ipropd_slave =~ /^enable/) {
    $service_ipropd_slave_enabled = true
  } else {
    fail("unrecognized value '${service_ipropd_slave}' for parameter service_ipropd_slave")
  }

  # Masters get the ipropd_master service.
  if ($service_ipropd_master == undef) {
    if ($master) {
      $service_ipropd_master_enabled = true
    } else {
      $service_ipropd_master_enabled = false
    }
  } elsif ($service_ipropd_master =~ /^disable/) {
    $service_ipropd_master_enabled = false
  } elsif ($service_ipropd_master =~ /^enable/) {
    $service_ipropd_master_enabled = true
  } else {
    fail("unrecognized value '${service_ipropd_master}' for parameter service_ipropd_master")
  }

  # Slaves get the hpropd service.
  if ($service_hpropd == undef) {
    if ($slave) {
      $service_hpropd_enabled = true
    } else {
      $service_hpropd_enabled = false
    }
  } elsif ($service_hpropd =~ /^disable/) {
    $service_hpropd_enabled = false
  } elsif ($service_hpropd =~ /^enable/) {
    $service_hpropd_enabled = true
  } else {
    fail("unrecognized value '${service_hpropd}' for parameter service_hpropd")
  }

  # Masters get the kadmind service.
  if ($service_kadmind == undef) {
    if ($master) {
      $service_kadmind_enabled = true
    } else {
      $service_kadmind_enabled = false
    }
  } elsif ($service_kadmind =~ /^disable/) {
    $service_kadmind_enabled = false
  } elsif ($service_kadmind =~ /^enable/) {
    $service_kadmind_enabled = true
  } else {
    fail("unrecognized value '${service_kadmind}' for parameter service_kadmind")
  }

  # Masters get the kpasswd service.
  if ($service_kpasswdd == undef) {
    if ($master) {
      $service_kpasswdd_enabled = true
    } else {
      $service_kpasswdd_enabled = false
    }
  } elsif ($service_kpasswdd =~ /^disable/) {
    $service_kpasswdd_enabled = false
  } elsif ($service_kpasswdd =~ /^enable/) {
    $service_kpasswdd_enabled = true
  } else {
    fail("unrecognized value '${service_kpasswdd}' for parameter service_kpasswdd")
  }

  # Masters get the service_remctl_password service.
  if ($service_remctl_password == undef) {
    if ($master) {
      $service_remctl_password_enabled = true
    } else {
      $service_remctl_password_enabled = false
    }
  } elsif ($service_remctl_password =~ /^disable/) {
    $service_remctl_password_enabled = false
  } elsif ($service_remctl_password =~ /^enable/) {
    $service_remctl_password_enabled = true
  } else {
    fail("unrecognized value '${service_remctl_password}' for parameter service_remctl_password")
  }

  # Masters get the ad sync service.
  if ($service_ad_sync == undef) {
    if ($master) {
      $service_ad_sync_enabled = true
    } else {
      $service_ad_sync_enabled = false
    }
  } elsif ($service_ad_sync =~ /^disable/) {
    $service_ad_sync_enabled = false
  } elsif ($service_ad_sync =~ /^enable/) {
    $service_ad_sync_enabled = true
  } else {
    fail("unrecognized value '${service_ad_sync}' for parameter service_ad_sync")
  }
  ##########################################################

  # We build our own kerberos packages, so set up our APT preferences
  # first. Note that this should be run _before_ any of the package
  # installs.
  class { 'kdc::apt':
    before => Class['Kdc::Packages'],
  }

  # Install kdc-specific packages
  include kdc::packages

  # Do we make nightly local backups of the kerberos database?
  if ($local_backup) {
    # Until we figure out how to get the backup password provisioned to a
    # container, we don't do encrypted backups in the container hosting
    # model.
    case $hosting_model {
      'traditional': {
        # We only backup the Heimdal password history file on the
        # master.
        if ($master) {
          $history_cleartext_backups = true
          $history_encrypted_backups = true
        } else {
          $history_cleartext_backups = false
          $history_encrypted_backups = false
        }
        class {'kdc::backup':
          env               => $env,
          cleartext_backups => true,
          encrypted_backups => true,
          copy_to_google    => $copy_to_google,
          backups_to_keep   => $backups_to_keep,
          history_cleartext_backups => $history_cleartext_backups,
          history_encrypted_backups => $history_encrypted_backups,
        }
      }
      'container': {
        class {'kdc::backup':
          env               => $env,
          cleartext_backups => true,
          encrypted_backups => false,
          copy_to_google    => $copy_to_google,
        }
      }
    }
  }

  # Load the configuration
  class { 'kdc::config':
    ensure                   => 'present',
    env                      => $env,
    realm                    => $realm,
    master                   => $master,
    slave                    => $slave,
    master_fqdn              => $master_fqdn,
    krb5_port                => $krb5_port,
    self_client              => $self_client,
    kdcs                     => $kdcs,
    service_ad_sync_enabled  => $service_ad_sync_enabled,
    sync_ad_realm            => $sync_ad_realm,
    sync_ad_upn_realm        => $sync_ad_upn_realm,
    sync_ad_admin_server     => $sync_ad_admin_server,
    sync_ad_ldap_base        => $sync_ad_ldap_base,
    sync_ad_keytab           => $sync_ad_keytab,
    sync_ad_base_instance    => $sync_ad_base_instance,
    sync_ad_queue_only       => $sync_ad_queue_only,
    krb5_sync_timer          => $krb5_sync_timer,
    krb5_sync_silent         => $krb5_sync_silent,
    krb5_sync_process_dir    => $krb5_sync_process_dir,
    krb5_sync_ldap_master    => $krb5_sync_ldap_master,
    slave_ipropd_keytab      => $slave_ipropd_keytab,
    master_ipropd_keytab     => $master_ipropd_keytab,
    using_base_kerberos      => $using_base_kerberos,
    kadmind_acl_extras       => $kadmind_acl_extras,
    logging_kdc              => $logging_kdc,
    logging_ipropd_slave     => $logging_ipropd_slave,
    logging_ipropd_master    => $logging_ipropd_master,
    logging_default          => $logging_default,
    extra_ip_to_hostnames    => $extra_ip_to_hostnames,
    service_kadmind_enabled  => $service_kadmind_enabled,
    password_quality_progam  => $password_quality_progam,
    winuat_domain_controller => $winuat_domain_controller,
  }

  # Load the master class.
  if ($master) {
    class { 'kdc::master':
      ensure                  => present,
      slaves                  => $slaves,
      service_ad_sync_enabled => $service_ad_sync_enabled,
      krb5_sync_purge_days    => $krb5_sync_purge_days,
      krb5_sync_dir           => $krb5_sync_dir,
      krb5_sync_process_dir   => $krb5_sync_process_dir,
    }
  } else {
    class { 'kdc::master':
      ensure => absent,
      slaves => $slaves,
    }
  }

  if ($use_filtersyslog) {
    include base::filtersyslog
  }

  if ($hosting_model == 'traditional') {
    class { 'kdc::traditional':
      env                           => $env,
      master                        => $master,
      slave                         => $slave,
      service_heimdal_kdc_enabled   => $service_heimdal_kdc_enabled,
      service_ipropd_master_enabled => $service_ipropd_master_enabled,
      service_ipropd_slave_enabled  => $service_ipropd_slave_enabled,
      service_kpasswdd_enabled      => $service_kpasswdd_enabled,
      service_hpropd_enabled        => $service_hpropd_enabled,
      master_ip                     => $master_ip,
      krb5_port                     => $krb5_port,
      master_fqdn                   => $master_fqdn,
      slaves                        => $slaves,
      slave_cidrs                   => $slave_cidrs,
      trusted_kadmin_cidrs          => $trusted_kadmin_cidrs,
      master_ipropd_keytab          => $master_ipropd_keytab,
      slave_ipropd_keytab           => $slave_ipropd_keytab,
      extra_sshd_hosts_allow        => $extra_sshd_hosts_allow,
#      wallet_server_backups         => $wallet_server_backups,
      extra_hpropd_cidrs            => $extra_hpropd_cidrs,
      master_time_gone_secs         => $master_time_gone_secs,
      master_time_missing_secs      => $master_time_missing_secs,
      slave_time_lost_secs          => $slave_time_lost_secs,
      nagios_ips                    => $nagios_ips,
    }
  }

  ## SERVICES

  if ($service_heimdal_kdc_enabled) {
    include kdc::service::heimdal_kdc
  }

  if ($service_ipropd_slave_enabled) {
    class { 'kdc::service::ipropd_slave':
      ensure     => present,
      nagios_ips => $nagios_ips,
    }
  } else {
    class { 'kdc::service::ipropd_slave':
      ensure => absent
    }
  }

  if ($service_hpropd_enabled) {
    class { 'kdc::service::hpropd':
      slave_ipropd_keytab => $slave_ipropd_keytab,
    }
  }

  if ($service_kadmind_enabled) {
    include kdc::service::kadmind
  }

  if ($service_remctl_password_enabled) {
    include kdc::service::remctl_password
  }

  ##

  # We include the heimdal-kdc filter-syslog files even if we do not use
  # filter-syslog (it doesn't hurt).
  file { '/etc/filter-syslog/heimdal-kdc':
    source => 'puppet:///modules/kdc/etc/filter-syslog/heimdal-kdc';
  }

  # Filter some of the output of krb5-sync. We include this on all
  # KDCs even those not running krb5-sync (it is tiny).
  file { '/etc/filter-syslog/krb5-sync':
    source => 'puppet:///modules/kdc/etc/filter-syslog/krb5-sync';
  }

  # Filter some remctl commands that run on KDCs.
  file { '/etc/filter-syslog/remctl-kdc':
    source => 'puppet:///modules/kdc/etc/filter-syslog/remctl-kdc';
  }

  # Include the ncsa client for passive Nagios monitors for all KDCs.
  package { 'nsca-client':
    ensure => installed
  }

}

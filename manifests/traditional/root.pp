# Set up the root users in /root/.k5login for a traditional server. This
# is really only relevant for the servers that get quarterly database
# refreshes from production, that is, prod, uat, and test.

# Override the default user::root class to restrict which users can be root.
class kdc::traditional::root inherits user::root {

  K5login['/root/.k5login'] {
    principals =>
      [
        'adamhl/admin@stanford.edu',
        'ljlgeek/admin@stanford.edu',
        'vivienwu/admin@stanford.edu',
      ]
  }

  K5login['/etc/remctl/acl/systems-root'] {
    principals =>
      [
        'adamhl/admin@stanford.edu',
        'ljlgeek/admin@stanford.edu',
        'vivienwu/admin@stanford.edu',
      ]
  }

}

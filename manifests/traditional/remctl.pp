# Restrict who can connect to the remctl service.
class kdc::traditional::remctl (
  $extra_cidrs = [],
) inherits base::remctl {

  $all_cidrs = [ '171.64.0.0/14', '204.63.224.0/21', '172.20.196.23' ] + $extra_cidrs

  # We also let in some accounts servers
  Base::Iptables::Rule['remctl'] {
    source => $all_cidrs,
  }

}
